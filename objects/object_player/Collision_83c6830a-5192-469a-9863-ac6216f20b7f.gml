/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 1A0910CD
/// @DnDArgument : "code" "//what type of pickup did it collide with?$(13_10)if (other.type == "points")$(13_10){$(13_10)	//add score$(13_10)	points = other.image_index * 10;$(13_10)	game_controller.p_score += 100;$(13_10)}$(13_10)else if (other.type == "health")$(13_10){$(13_10)	//restore health$(13_10)	game_controller.p_health = 100;$(13_10)}$(13_10)else if (other.type == "lives")$(13_10){$(13_10)	game_controller.p_lives += 1;$(13_10)}$(13_10)else if (other.type == "damage")$(13_10){$(13_10)	game_controller.p_health -= damage;$(13_10)}$(13_10)else if (other.type == "death")$(13_10){$(13_10)	game_controller.p_health = 0;$(13_10)}$(13_10)//remove the pickup (destroy it)$(13_10)instance_destroy(other);$(13_10)"
//what type of pickup did it collide with?
if (other.type == "points")
{
	//add score
	points = other.image_index * 10;
	game_controller.p_score += 100;
}
else if (other.type == "health")
{
	//restore health
	game_controller.p_health = 100;
}
else if (other.type == "lives")
{
	game_controller.p_lives += 1;
}
else if (other.type == "damage")
{
	game_controller.p_health -= damage;
}
else if (other.type == "death")
{
	game_controller.p_health = 0;
}
//remove the pickup (destroy it)
instance_destroy(other);