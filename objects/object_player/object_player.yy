{
    "id": "b0b718af-d59e-4996-810d-3cc0b475a871",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_player",
    "eventList": [
        {
            "id": "dc409031-4a6f-4d38-8c9c-245b936b071c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b0b718af-d59e-4996-810d-3cc0b475a871"
        },
        {
            "id": "14ed9b00-9087-4f97-9467-9b6e21368245",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "b0b718af-d59e-4996-810d-3cc0b475a871"
        },
        {
            "id": "d7563285-bcb9-4298-8ced-77b55430e4b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 5,
            "m_owner": "b0b718af-d59e-4996-810d-3cc0b475a871"
        },
        {
            "id": "91902d10-831b-4144-a95c-93d3f815d0a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "b0b718af-d59e-4996-810d-3cc0b475a871"
        },
        {
            "id": "559f05c4-32ab-41f9-9811-3e985b996cd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "b0b718af-d59e-4996-810d-3cc0b475a871"
        },
        {
            "id": "fa9251cd-bcad-4f32-8bd8-92c3afc9136e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "b0b718af-d59e-4996-810d-3cc0b475a871"
        },
        {
            "id": "da72a951-50a7-441a-9445-1f809bff68e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cedbf62c-0b71-449b-a838-2d10325bcdc6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b0b718af-d59e-4996-810d-3cc0b475a871"
        },
        {
            "id": "1be8dcc2-c6be-4761-9535-6ae43257cd59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1d0fa378-d05b-4494-a1ad-4a5e167835ac",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b0b718af-d59e-4996-810d-3cc0b475a871"
        },
        {
            "id": "0b0c2cb3-a9fa-4b2c-9c73-0e15b7c65972",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 33,
            "eventtype": 9,
            "m_owner": "b0b718af-d59e-4996-810d-3cc0b475a871"
        },
        {
            "id": "34cd52b8-a081-41cb-bd04-3bb5916a595c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 34,
            "eventtype": 9,
            "m_owner": "b0b718af-d59e-4996-810d-3cc0b475a871"
        },
        {
            "id": "617bc58f-b9ce-4a06-ac3d-f052081a8125",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "6081ed3d-f7d7-4f2c-9874-dd99e1610b4e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b0b718af-d59e-4996-810d-3cc0b475a871"
        },
        {
            "id": "6c3ac24a-b78a-4d1e-9c3f-c2c707ca89dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b0b718af-d59e-4996-810d-3cc0b475a871"
        },
        {
            "id": "d014e99e-0ae4-4de5-be6c-c2c2cbd9c7b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "12edf28b-5ad8-4186-aaa9-d0d6390a5138",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b0b718af-d59e-4996-810d-3cc0b475a871"
        },
        {
            "id": "83c6830a-5192-469a-9863-ac6216f20b7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "4ec10395-2ac5-484a-93bd-dbdc51007394",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b0b718af-d59e-4996-810d-3cc0b475a871"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c7a4fb25-0a48-442e-9695-379f30c412e8",
    "visible": true
}