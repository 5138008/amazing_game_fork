/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 3B23E4EC
/// @DnDDisabled : 1
/// @DnDArgument : "var" ""


/// @DnDAction : YoYo Games.Instances.Create_Instance
/// @DnDVersion : 1
/// @DnDHash : 1C9218CC
/// @DnDArgument : "xpos" " x"
/// @DnDArgument : "ypos" "y"
/// @DnDArgument : "objectid" "object_bomb"
/// @DnDSaveInfo : "objectid" "0b59a7ba-24ec-458c-adb8-0a82ccba9c1b"
instance_create_layer( x, y, "Instances", object_bomb);

/// @DnDAction : YoYo Games.Movement.Set_Direction_Free
/// @DnDVersion : 1
/// @DnDHash : 54B3EC35
/// @DnDArgument : "direction" "image_angle"
direction = image_angle;

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 2A077D45
/// @DnDDisabled : 1
/// @DnDArgument : "code" "if (keyboard_check_pressed(vk_space))$(13_10){$(13_10)var inst = instance_create_layer(x, y, "Instances", obj_bullet);$(13_10)inst.direction = image_angle;$(13_10)}"