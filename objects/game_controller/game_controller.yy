{
    "id": "352221c3-39ce-41eb-a562-d9cd79948ff3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "game_controller",
    "eventList": [
        {
            "id": "7956f74d-d3f2-4836-ab73-cd12f5c8bcbd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "352221c3-39ce-41eb-a562-d9cd79948ff3"
        },
        {
            "id": "4eff136c-516e-4fcf-9530-ab45131ed807",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "352221c3-39ce-41eb-a562-d9cd79948ff3"
        },
        {
            "id": "66103786-3909-4f9c-be1c-ad57b4cab7dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "352221c3-39ce-41eb-a562-d9cd79948ff3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}