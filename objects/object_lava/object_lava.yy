{
    "id": "56fa0b8d-3173-4876-8ad6-a4efae9f79e1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_lava",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6081ed3d-f7d7-4f2c-9874-dd99e1610b4e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c0733b4f-c0c2-4cfd-9021-1babdbe621ad",
    "visible": true
}