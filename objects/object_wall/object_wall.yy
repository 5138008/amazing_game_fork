{
    "id": "cedbf62c-0b71-449b-a838-2d10325bcdc6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_wall",
    "eventList": [
        {
            "id": "5d999307-9382-44eb-b382-df15f8b6c8d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cedbf62c-0b71-449b-a838-2d10325bcdc6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "754c1ee0-8517-4ec8-b61a-693bc9bf9416",
    "visible": true
}