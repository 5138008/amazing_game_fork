{
    "id": "5e91e49b-de45-4752-bd64-22c7cbc4dbc2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_skeleton",
    "eventList": [
        {
            "id": "316a8e4e-08c8-4003-9e71-faea62be504a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5e91e49b-de45-4752-bd64-22c7cbc4dbc2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6081ed3d-f7d7-4f2c-9874-dd99e1610b4e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "06040f06-d926-4e9c-9172-568db4252b60",
    "visible": true
}