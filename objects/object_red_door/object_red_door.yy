{
    "id": "12edf28b-5ad8-4186-aaa9-d0d6390a5138",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_red_door",
    "eventList": [
        {
            "id": "20d45c1b-1bb4-4464-9a65-d75a046fc8d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "12edf28b-5ad8-4186-aaa9-d0d6390a5138"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "f4c03e0d-4b2a-4733-8d15-d148e01f7480",
    "visible": true
}