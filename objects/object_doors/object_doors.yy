{
    "id": "1d0fa378-d05b-4494-a1ad-4a5e167835ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_doors",
    "eventList": [
        {
            "id": "eadff477-6154-407c-864c-c477cf8e848a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "b0b718af-d59e-4996-810d-3cc0b475a871",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1d0fa378-d05b-4494-a1ad-4a5e167835ac"
        },
        {
            "id": "067d4a6b-0434-4d84-92b8-37d396e1fc98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1d0fa378-d05b-4494-a1ad-4a5e167835ac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f4c03e0d-4b2a-4733-8d15-d148e01f7480",
    "visible": true
}