{
    "id": "4ec10395-2ac5-484a-93bd-dbdc51007394",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_pickup",
    "eventList": [
        {
            "id": "0799f2f9-ff36-430c-b1cc-eabeed5de085",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4ec10395-2ac5-484a-93bd-dbdc51007394"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e095efab-2606-466f-83a2-e8080f7740a7",
    "visible": true
}