{
    "id": "29f02538-a7c6-4ef0-868c-f65ac16be31f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_red_switch",
    "eventList": [
        {
            "id": "322d3879-57b2-43b9-b595-b7ff522d869d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "29f02538-a7c6-4ef0-868c-f65ac16be31f"
        },
        {
            "id": "48a80e78-6888-49ac-94cc-91496ffdb14d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "b0b718af-d59e-4996-810d-3cc0b475a871",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "29f02538-a7c6-4ef0-868c-f65ac16be31f"
        },
        {
            "id": "6599d3f1-8ddb-4d51-88c6-706e2562f4a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "29f02538-a7c6-4ef0-868c-f65ac16be31f"
        },
        {
            "id": "d6ee4045-43cf-4533-b069-abb3367f1c83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "29f02538-a7c6-4ef0-868c-f65ac16be31f"
        },
        {
            "id": "664e3bc8-1443-4a70-946a-bba3037a8fa0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "29f02538-a7c6-4ef0-868c-f65ac16be31f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "1a2d1e0d-70ad-4f9c-89c9-491da146c8ec",
    "visible": true
}