/// @DnDAction : YoYo Games.Instances.Set_Sprite
/// @DnDVersion : 1
/// @DnDHash : 2E480290
/// @DnDArgument : "imageind" "1"
/// @DnDArgument : "spriteind" "sprite_red_switch"
/// @DnDSaveInfo : "spriteind" "1a2d1e0d-70ad-4f9c-89c9-491da146c8ec"
sprite_index = sprite_red_switch;
image_index = 1;

/// @DnDAction : YoYo Games.Instances.Set_Alarm
/// @DnDVersion : 1
/// @DnDHash : 4A1F3443
/// @DnDArgument : "steps" "30*10"
alarm_set(0, 30*10);

/// @DnDAction : YoYo Games.Movement.Jump_To_Point
/// @DnDVersion : 1
/// @DnDHash : 3BB0044F
/// @DnDApplyTo : 12edf28b-5ad8-4186-aaa9-d0d6390a5138
/// @DnDArgument : "x" "0"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "2000"
/// @DnDArgument : "y_relative" "1"
with(object_red_door) {
x += 0;
y += 2000;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 23D60945
/// @DnDArgument : "var" "canPlaySound"
/// @DnDArgument : "value" "true"
if(canPlaySound == true)
{
	/// @DnDAction : YoYo Games.Audio.Play_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 404211D7
	/// @DnDParent : 23D60945
	/// @DnDArgument : "soundid" "sound_door"
	/// @DnDSaveInfo : "soundid" "1c08938a-a2a2-49e2-8e97-b0e076b30788"
	audio_play_sound(sound_door, 0, 0);

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 2477622D
	/// @DnDParent : 23D60945
	/// @DnDArgument : "expr" " false"
	/// @DnDArgument : "var" "canPlaySound"
	canPlaySound =  false;
}