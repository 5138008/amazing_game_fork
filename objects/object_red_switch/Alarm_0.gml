/// @DnDAction : YoYo Games.Instances.Set_Sprite
/// @DnDVersion : 1
/// @DnDHash : 26294FEA
/// @DnDArgument : "spriteind" "sprite_red_switch"
/// @DnDSaveInfo : "spriteind" "1a2d1e0d-70ad-4f9c-89c9-491da146c8ec"
sprite_index = sprite_red_switch;
image_index = 0;

/// @DnDAction : YoYo Games.Audio.Play_Audio
/// @DnDVersion : 1
/// @DnDHash : 0DD29DB2
/// @DnDArgument : "soundid" "sound_door"
/// @DnDSaveInfo : "soundid" "1c08938a-a2a2-49e2-8e97-b0e076b30788"
audio_play_sound(sound_door, 0, 0);

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 2F832847
/// @DnDArgument : "expr" "true"
/// @DnDArgument : "var" "can"
can = true;

/// @DnDAction : YoYo Games.Movement.Jump_To_Point
/// @DnDVersion : 1
/// @DnDHash : 76919C4B
/// @DnDApplyTo : 12edf28b-5ad8-4186-aaa9-d0d6390a5138
/// @DnDArgument : "x" "193"
/// @DnDArgument : "y" "387"
with(object_red_door) {
x = 193;
y = 387;
}