{
    "id": "0b59a7ba-24ec-458c-adb8-0a82ccba9c1b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_bomb",
    "eventList": [
        {
            "id": "e1596b62-06f7-47c3-af2e-66e44c2f6459",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0b59a7ba-24ec-458c-adb8-0a82ccba9c1b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "21eb7f02-b739-469f-b331-9eeb7a7d0f41",
    "visible": true
}