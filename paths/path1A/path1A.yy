{
    "id": "6eba337b-8428-4fd6-b4be-94be67009a15",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path1A",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "61694105-ce55-488e-8c87-5c6837540dbe",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 448,
            "speed": 100
        },
        {
            "id": "7c4d8a8f-6cfe-48f6-b802-f255e394b142",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 512,
            "speed": 100
        },
        {
            "id": "126bee06-1618-4d53-91e0-d092e0be4b52",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 544,
            "speed": 100
        },
        {
            "id": "0692b6b5-7b22-4f38-bb25-9f5f8c52df2f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 608,
            "speed": 100
        },
        {
            "id": "8adc90f5-11e2-4d32-97b6-c5e865cf81ba",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 672,
            "speed": 100
        },
        {
            "id": "13576947-f120-48c7-bbec-768172a2183d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 704,
            "speed": 100
        },
        {
            "id": "f7da24f1-4b57-4915-a369-05150d833102",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 704,
            "speed": 100
        },
        {
            "id": "11748f81-9e72-47d7-8f53-54130d9c17d5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 672,
            "speed": 100
        },
        {
            "id": "520560c3-36eb-4861-8642-f709ce230cff",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 576,
            "speed": 100
        },
        {
            "id": "90b8f0cf-0071-47fb-8c99-75bd4c50ac1e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 576,
            "speed": 100
        },
        {
            "id": "fc6b77bd-3349-480e-9995-a0b1a4cc963b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 640,
            "speed": 100
        },
        {
            "id": "543e3d90-c61b-49ba-9d4c-3e33fd55dfeb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 704,
            "speed": 100
        },
        {
            "id": "7db9083d-72fe-49d0-8d4a-bb13628a3d06",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 480,
            "speed": 100
        },
        {
            "id": "9560e3ec-4034-48b7-b026-cc326b64bb1b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 416,
            "speed": 100
        },
        {
            "id": "d8a61e0a-1c2c-42f7-97bc-ec1601164425",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 96,
            "y": 384,
            "speed": 100
        },
        {
            "id": "dad11311-e1bd-4971-8345-00b0ef979c64",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 320,
            "speed": 100
        },
        {
            "id": "7332b8f6-a4c9-4280-ba89-6771890270ab",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 192,
            "speed": 100
        },
        {
            "id": "73121773-d43a-4dae-9e91-5e22cde00085",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 192,
            "speed": 100
        },
        {
            "id": "c6481cad-5c69-48cf-971c-ff2e964ffe5d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 192,
            "speed": 100
        },
        {
            "id": "d7e10e7e-293b-4216-8ec4-022b677320bf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 224,
            "speed": 100
        },
        {
            "id": "0734efd8-28e1-40c1-bb85-21106243981a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 256,
            "speed": 100
        },
        {
            "id": "8912691e-48eb-4f17-8ee2-bd2367123070",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 288,
            "speed": 100
        },
        {
            "id": "f251904e-6a8b-42b7-b348-f59825aae46c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 320,
            "speed": 100
        },
        {
            "id": "5368bae5-00bb-4cdd-b58a-7cc117a26d80",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 352,
            "speed": 100
        },
        {
            "id": "bbb21f3b-59ed-4ce6-b0ab-43ec289be511",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 448,
            "speed": 100
        },
        {
            "id": "293102df-02c5-4177-859a-0367286491fb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 544,
            "speed": 100
        },
        {
            "id": "45855b9a-3212-43f0-a715-b853eda084af",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 704,
            "speed": 100
        },
        {
            "id": "e0097a4f-4321-4710-87ce-6f34672415b7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 736,
            "speed": 100
        },
        {
            "id": "df802c01-6b64-49be-b14c-8812a450d563",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 576,
            "speed": 100
        },
        {
            "id": "ad4bd55c-2092-432f-8ae9-2eae99681a74",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 480,
            "speed": 100
        },
        {
            "id": "40a2d48f-21fb-45a3-986e-b0258d362ae0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 384,
            "speed": 100
        },
        {
            "id": "b0cc33d6-3b01-4a4f-b763-9923844bd92f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 288,
            "speed": 100
        },
        {
            "id": "49f53aa3-627a-4dbf-a512-a6e4cdf7f0c4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 288,
            "speed": 100
        },
        {
            "id": "4ec54b69-9238-4e9a-838b-2413f497149f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 480,
            "speed": 100
        },
        {
            "id": "60d5ec33-a21e-4505-abfd-504cde562785",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 480,
            "speed": 100
        },
        {
            "id": "7fe73e75-1bbb-4636-ba47-1552cea8d95f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 512,
            "speed": 100
        },
        {
            "id": "8d85f8d2-ba2b-480b-96ea-1beb2ded0b3d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 640,
            "speed": 100
        },
        {
            "id": "40aa9f89-d33e-4dbc-9f61-5945095d7e4c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 704,
            "speed": 100
        },
        {
            "id": "55a62d82-0f64-44cd-a003-0bf1b46e48a5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 704,
            "speed": 100
        },
        {
            "id": "b2b3155c-1fdd-4a9f-89db-b95107aa3814",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 704,
            "speed": 100
        },
        {
            "id": "356a5c96-c745-4664-993e-f38402b6d923",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 608,
            "speed": 100
        },
        {
            "id": "87a5717e-0364-461f-854c-0310234f25ec",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 512,
            "speed": 100
        },
        {
            "id": "7e25b4ed-4d4b-4a71-a485-39558149b2ec",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 384,
            "speed": 100
        },
        {
            "id": "8976ecab-6f99-4812-a80e-1a458f370062",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 896,
            "y": 352,
            "speed": 100
        },
        {
            "id": "0fc2c9f0-3513-4251-9cea-0ecce94bd050",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 352,
            "speed": 100
        },
        {
            "id": "27d965d8-1d71-47da-9750-27652650442f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 224,
            "speed": 100
        },
        {
            "id": "68700f84-055e-406a-b5da-e74f50eb9f64",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 32,
            "speed": 100
        },
        {
            "id": "b40448e3-1a24-40cb-8752-b08d233abf0c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 160,
            "speed": 100
        },
        {
            "id": "f8f0acf2-8c24-4748-8779-2fb953737117",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 224,
            "speed": 100
        },
        {
            "id": "61e2459e-e020-432e-8acd-2ec5b74df173",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 288,
            "speed": 100
        },
        {
            "id": "943ac8ee-b036-4815-bd36-6c969434d88f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 416,
            "speed": 100
        },
        {
            "id": "218115dd-b95b-4566-a0af-b6d595771cde",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 576,
            "speed": 100
        },
        {
            "id": "32a22261-8ae6-424f-891c-8b44350c8460",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 672,
            "speed": 100
        },
        {
            "id": "597e3ac3-7d91-43f7-9071-0ade753b6d85",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 672,
            "speed": 100
        },
        {
            "id": "f6e1195e-4e74-4403-a512-33267ace456d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 704,
            "speed": 100
        },
        {
            "id": "68942852-0f4f-44ec-af0c-5ebe10bd9782",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 928,
            "y": 704,
            "speed": 100
        },
        {
            "id": "a85012f1-e582-40a8-a858-cd4c82a9bcc4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 480,
            "speed": 100
        },
        {
            "id": "0dffba07-8b58-4dd7-be69-4bf90a97e40f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 544,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}