{
    "id": "6f3c8821-f678-4176-a563-17c9994c239a",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path3C",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "843405b7-190b-4f64-a3d5-acc1606bf3a1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 640,
            "speed": 100
        },
        {
            "id": "a234714b-aaee-4f3f-8854-02d6e6c4b574",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 704,
            "speed": 100
        },
        {
            "id": "5c310bce-ba98-464b-afd3-ad1465849d86",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 704,
            "speed": 100
        },
        {
            "id": "cb5dce9a-d1ef-4606-a3ce-4ff015748f5a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 704,
            "speed": 100
        },
        {
            "id": "6396740f-fef9-443e-ab73-ea102498fa9c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 672,
            "speed": 100
        },
        {
            "id": "f7e2eadb-f659-469c-b584-31f81b4e29bd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 672,
            "speed": 100
        },
        {
            "id": "d2f94434-e84c-47b6-836f-adef89cab4ae",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 704,
            "speed": 100
        },
        {
            "id": "520accf0-6109-4503-86f6-174a439996ab",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 896,
            "y": 704,
            "speed": 100
        },
        {
            "id": "f0559337-ad6f-4726-a7b8-06de5565173a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 704,
            "speed": 100
        },
        {
            "id": "c4699b0f-6a8c-4c8b-ba89-abb3c6d64910",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 576,
            "speed": 100
        },
        {
            "id": "3ef4c0bf-73c1-4435-9ada-db5b4c58ba85",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 448,
            "speed": 100
        },
        {
            "id": "ecf325f6-a76b-4838-950c-799f83bc6fbe",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 384,
            "speed": 100
        },
        {
            "id": "0cfef17f-fb88-4b73-a87c-3b1f528e2537",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 384,
            "speed": 100
        },
        {
            "id": "09a52d56-36d0-406e-b4de-38c1ce583029",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 352,
            "speed": 100
        },
        {
            "id": "fee6a7c2-be82-426a-a037-466f265312e8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 320,
            "speed": 100
        },
        {
            "id": "ba477604-e9bd-4229-a7f5-5bf1b9e6f39d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 416,
            "speed": 100
        },
        {
            "id": "d1d84acf-83f2-4c1f-aed3-cd83f617e7ce",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 512,
            "speed": 100
        },
        {
            "id": "a7f804ea-18cc-48e8-84ff-5f572060134c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 608,
            "speed": 100
        },
        {
            "id": "f214a9f4-0c80-4b2e-85ec-dc35b92e6071",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 640,
            "speed": 100
        },
        {
            "id": "7fe693a0-318b-4876-a6ed-0fbbf6264223",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 704,
            "speed": 100
        },
        {
            "id": "1e11bcbc-c194-48d6-930d-7c29f9789b62",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 704,
            "speed": 100
        },
        {
            "id": "5c20d7b0-68d1-415e-96fb-6e76e0469a1c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 608,
            "speed": 100
        },
        {
            "id": "4476e923-db7b-4848-abd5-7288682d2ad7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 448,
            "speed": 100
        },
        {
            "id": "a1312e3c-ab92-49d2-8671-577c451173fb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 416,
            "speed": 100
        },
        {
            "id": "a603313b-de84-4f4c-b7c5-fbd1aabaa4da",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 640,
            "speed": 100
        },
        {
            "id": "89263dd2-bd7a-4f5d-bd67-00f4bae7fd98",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 384,
            "speed": 100
        },
        {
            "id": "f52e97e7-f5f9-48ad-9c8d-67dd6c553e63",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 160,
            "speed": 100
        },
        {
            "id": "25b0c1e3-1152-4c95-a550-da86866048b0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 128,
            "speed": 100
        },
        {
            "id": "6320393c-2b4c-4080-8a55-aff8b05930b6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 192,
            "speed": 100
        },
        {
            "id": "89632da9-f4e4-40ab-afa9-325a0475d7e3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 160,
            "speed": 100
        },
        {
            "id": "a2c96d57-abd7-4f0f-b16d-1d2fe6d1213c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 64,
            "speed": 100
        },
        {
            "id": "a7ce76f8-ae4b-46f6-92ed-4a50528377f5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 32,
            "speed": 100
        },
        {
            "id": "3f2ed620-25d2-46c6-b3ed-05b97fcedd90",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 32,
            "speed": 100
        },
        {
            "id": "84946f8c-9ad9-42e5-a158-be20caf9fb82",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 96,
            "speed": 100
        },
        {
            "id": "6e99dc0c-839e-49f7-90f3-dc71b40ce06e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 224,
            "speed": 100
        },
        {
            "id": "24d140ea-4be5-4a12-ba8a-e5818f4b3ff8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 384,
            "speed": 100
        },
        {
            "id": "5f285d03-7f03-4aba-981d-2e0901ad499f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 576,
            "speed": 100
        },
        {
            "id": "9a47a0b3-8915-46e5-8276-10a834563aa1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 544,
            "speed": 100
        },
        {
            "id": "762c76f8-2e00-4eee-a6c3-66b43b93cec1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 352,
            "speed": 100
        },
        {
            "id": "cdb59e52-5f8c-4f0b-8039-47dde2b3836e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 160,
            "speed": 100
        },
        {
            "id": "5cdc84e1-8ce2-4e37-b2a6-55dcb43ee145",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 128,
            "speed": 100
        },
        {
            "id": "8b83d9bf-19b1-46cd-8842-d4bd834f2816",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 96,
            "speed": 100
        },
        {
            "id": "a1114113-8fbe-41da-a5d0-17862a9edc36",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 64,
            "speed": 100
        },
        {
            "id": "0aa7498f-8478-4c6d-81e1-a34228badd1d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 32,
            "speed": 100
        },
        {
            "id": "24eba22d-c8e7-481f-8ac9-b8879fdc7760",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 64,
            "speed": 100
        },
        {
            "id": "2606d6d1-b70d-4d04-b50a-da5819af4a11",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 96,
            "speed": 100
        },
        {
            "id": "d71d834d-4128-4a52-8784-014cf19bcf38",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 160,
            "speed": 100
        },
        {
            "id": "c92c8e77-0819-42df-b72e-45e1cd49674c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 192,
            "speed": 100
        },
        {
            "id": "66acf12a-3866-4691-ab55-cddf3210ab4e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 224,
            "speed": 100
        },
        {
            "id": "904c3833-4175-4b43-bbb2-1e62eb61816f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 256,
            "speed": 100
        },
        {
            "id": "4fc19b4d-d682-4dd2-8b97-ee2fa4561666",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 928,
            "y": 288,
            "speed": 100
        },
        {
            "id": "9f5da68e-54a3-4b15-bf4b-d9747e0807ad",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 320,
            "speed": 100
        },
        {
            "id": "c43dd26e-04c2-457c-8bb3-d60a012e8b2d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 896,
            "y": 352,
            "speed": 100
        },
        {
            "id": "093e906a-5867-4cf0-a675-c8ae5008faca",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 352,
            "speed": 100
        },
        {
            "id": "49d192a5-c8f2-49ea-a898-2dfce7ef17b7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 928,
            "y": 384,
            "speed": 100
        },
        {
            "id": "d57d6a55-6262-4dc8-8e1a-30f2fb7c0015",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 416,
            "speed": 100
        },
        {
            "id": "0336d196-c52a-4fcb-ab43-abf8a8d90331",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 928,
            "y": 448,
            "speed": 100
        },
        {
            "id": "5fdc42e9-9339-4c07-8dac-b7b8ae3994ab",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 480,
            "speed": 100
        },
        {
            "id": "bd7c70fe-6f74-45b2-b571-5892dc3b51e8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 928,
            "y": 480,
            "speed": 100
        },
        {
            "id": "32912384-3a85-4950-8a4c-b3f1cd8e67b8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 512,
            "speed": 100
        },
        {
            "id": "ad468ba7-c253-4bed-ac8d-dde6ee8eb27e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 928,
            "y": 512,
            "speed": 100
        },
        {
            "id": "a2de1e7d-a8c1-47f5-94df-721f372a2ca6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 544,
            "speed": 100
        },
        {
            "id": "d25a315f-7dd3-42e5-8e34-ea503176b5a9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 928,
            "y": 544,
            "speed": 100
        },
        {
            "id": "56bfd62b-c063-4a16-91e1-38ad21f8657f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 576,
            "speed": 100
        },
        {
            "id": "122616f3-b35a-485b-bac5-c35918fc38fd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 608,
            "speed": 100
        },
        {
            "id": "a80696d3-2c2b-43b3-98f6-3a977eb417e7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 928,
            "y": 608,
            "speed": 100
        },
        {
            "id": "340a0321-2940-47d4-b1e6-1bc5aec78f75",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 928,
            "y": 576,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}