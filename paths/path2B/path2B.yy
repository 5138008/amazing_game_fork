{
    "id": "4435353a-51f0-4203-9601-858b3bf2fddc",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path2B",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "4586974a-a427-49e1-846e-6cc595243302",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 480,
            "speed": 100
        },
        {
            "id": "fc7f4586-d36f-4f43-bc5b-c85dbcf68a4c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 576,
            "speed": 100
        },
        {
            "id": "b297a7bf-5f1f-4d8c-8cc5-cd2612452ed2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 96,
            "y": 576,
            "speed": 100
        },
        {
            "id": "7466309f-78ea-4838-bfd6-8e7c6f542527",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 576,
            "speed": 100
        },
        {
            "id": "4260c959-8a31-4ddb-b25c-c1c26acec9db",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 416,
            "speed": 100
        },
        {
            "id": "bba8cd92-a4a8-4859-9dc2-0d6145ab087e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 320,
            "speed": 100
        },
        {
            "id": "cb845a2d-564d-40dc-9273-22ff65f98a8e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 352,
            "speed": 100
        },
        {
            "id": "c1a34d9f-2715-4421-a8a9-1c63560c26ef",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 416,
            "speed": 100
        },
        {
            "id": "1865f96a-94cc-4368-a2d9-3c30d29ac932",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 480,
            "speed": 100
        },
        {
            "id": "b6613445-918c-4ccb-8796-377407d8b1a0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 480,
            "speed": 100
        },
        {
            "id": "5208f279-f6df-481d-a36b-b99bc50f8dbb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 448,
            "speed": 100
        },
        {
            "id": "fa272dd2-c55a-4c2a-a414-8328df88c706",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 480,
            "speed": 100
        },
        {
            "id": "b96bd0c8-2c2a-4f59-bbd2-d21199cb8945",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 480,
            "speed": 100
        },
        {
            "id": "cf98716d-6a0c-430d-9926-5a46f09decd1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 480,
            "speed": 100
        },
        {
            "id": "774b66a5-f920-43e3-b87b-9d274da46ce9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 512,
            "speed": 100
        },
        {
            "id": "a9cc69fe-3513-4efd-8802-da173453ccf8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 576,
            "speed": 100
        },
        {
            "id": "9a43f9bf-44fe-47bf-8fcd-8cfbd0f6323d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 608,
            "speed": 100
        },
        {
            "id": "e2ac264c-9375-4232-baa1-56e6e4bc9686",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 576,
            "speed": 100
        },
        {
            "id": "8b001859-cdae-498c-9c14-3cdcc50384e5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 512,
            "speed": 100
        },
        {
            "id": "e6493043-97e8-4414-ae2f-6fec24efbd38",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 448,
            "speed": 100
        },
        {
            "id": "30831c6e-e83c-463b-9143-9f8730c85a0b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 480,
            "speed": 100
        },
        {
            "id": "2d4f1b52-ed31-4fd3-9b5a-b5f116b36014",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 512,
            "speed": 100
        },
        {
            "id": "528a3e03-2932-47ef-868e-dcf78d815faa",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 384,
            "speed": 100
        },
        {
            "id": "03dcda10-9ada-462a-917a-1e99c271e045",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 448,
            "speed": 100
        },
        {
            "id": "93a1c63f-69e5-4ba2-9da1-9069ca076557",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 448,
            "speed": 100
        },
        {
            "id": "66c7cc13-7316-4fde-bf4b-722fcc8ebf0a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 448,
            "speed": 100
        },
        {
            "id": "b9b5985d-da3e-434a-a89b-3066e33c9573",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 288,
            "speed": 100
        },
        {
            "id": "61f9fcc2-8f04-4f88-a4ae-17048e821bdf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 928,
            "y": 128,
            "speed": 100
        },
        {
            "id": "ba0eb7b2-e090-4fdd-97d3-2550d3c3e9eb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 96,
            "speed": 100
        },
        {
            "id": "067ff00b-1f0a-4902-8a11-1fe7691c1f9e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 64,
            "speed": 100
        },
        {
            "id": "0f612133-0770-4f43-bf0e-8935906aaf52",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 96,
            "speed": 100
        },
        {
            "id": "a41b1ecf-4c87-4b13-bfc4-67cefe6ffb95",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 160,
            "speed": 100
        },
        {
            "id": "d306749c-1a92-41b2-b521-1747dae05d81",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 256,
            "speed": 100
        },
        {
            "id": "9c29e3e1-a8ba-488e-9083-a6e67003d5fc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 320,
            "speed": 100
        },
        {
            "id": "a1f4de46-f265-454b-ab5f-a854cb44a93e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 320,
            "speed": 100
        },
        {
            "id": "55d510bb-ef7f-462e-8686-12242e886683",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 288,
            "speed": 100
        },
        {
            "id": "fa95b8f1-0a1b-4725-abc0-446964482eda",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 256,
            "speed": 100
        },
        {
            "id": "4f507702-5f3d-497b-8c47-4e2b4337dbda",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 64,
            "speed": 100
        },
        {
            "id": "b6569b11-5342-4a5f-8671-21dd6bb07dec",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 32,
            "speed": 100
        },
        {
            "id": "30a805ae-55d6-409a-af02-9eab0cb937e8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 32,
            "speed": 100
        },
        {
            "id": "b4f9db29-7000-41ab-b1fb-4747eccec298",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 160,
            "speed": 100
        },
        {
            "id": "9f51d621-d585-447c-b4a8-eb38f48b803e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 160,
            "speed": 100
        },
        {
            "id": "41987ae6-f035-4035-8492-49551d3d4c08",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 96,
            "speed": 100
        },
        {
            "id": "9b119ab4-6996-452e-a8a7-cb55efed28b1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 96,
            "speed": 100
        },
        {
            "id": "286e30e6-3cac-4a0e-94dd-4b4ac954a6d2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 96,
            "speed": 100
        },
        {
            "id": "e351061b-db5d-4f6c-bab4-d6bd56c6b4b9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 64,
            "speed": 100
        },
        {
            "id": "6d7208c6-286d-4964-9ad1-a242b674789c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 64,
            "speed": 100
        },
        {
            "id": "171441ff-017a-4e71-bc53-adbec14f03be",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 32,
            "speed": 100
        },
        {
            "id": "b237ae73-f3eb-48ed-8274-e7130898b331",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 160,
            "speed": 100
        },
        {
            "id": "2922657a-6cda-48ec-b886-16fc08d68615",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 288,
            "speed": 100
        },
        {
            "id": "30cf1088-9f28-4498-811b-ffc3133975ca",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 416,
            "speed": 100
        },
        {
            "id": "12ebded9-fc9a-4f26-ade6-183f1711b8ca",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 672,
            "speed": 100
        },
        {
            "id": "aeffa56b-0747-4762-912a-b2d29b5cf177",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 704,
            "speed": 100
        },
        {
            "id": "4cbff99f-47c5-4add-bb20-17ceda12eab4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 704,
            "speed": 100
        },
        {
            "id": "4f696248-0bb4-48ad-a432-8d2c53b041d8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 704,
            "speed": 100
        },
        {
            "id": "2bdf5dc3-b40c-451e-a22b-9d6085fac962",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 704,
            "speed": 100
        },
        {
            "id": "d5b9ae25-00d2-4ef5-a025-1a5361fc1e50",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 608,
            "speed": 100
        },
        {
            "id": "314082a6-3bd9-4588-b0f1-10789e84ffd0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 576,
            "speed": 100
        },
        {
            "id": "fe7f3a1c-3cd4-4037-8318-309229e36775",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 608,
            "speed": 100
        },
        {
            "id": "76e23935-0747-49a0-8b82-fcd0df8a64ed",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 640,
            "speed": 100
        },
        {
            "id": "816d4db7-ffdf-482a-a1fd-1ad37def9d64",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 704,
            "speed": 100
        },
        {
            "id": "38e0c062-78fc-4f16-a1bd-150dd48f335f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 640,
            "speed": 100
        },
        {
            "id": "d331894c-5f1d-4c40-a023-ade1e290b7cc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 608,
            "speed": 100
        },
        {
            "id": "e831b6cf-03de-4e0d-a9ad-729b0192e04e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 640,
            "speed": 100
        },
        {
            "id": "cfc759f8-56c5-43c5-902e-3e3795496695",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 576,
            "speed": 100
        },
        {
            "id": "27c6f575-722a-46f4-9e34-3567fb2e9d15",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 896,
            "y": 544,
            "speed": 100
        },
        {
            "id": "3dd7edf6-8e82-4cb9-9af1-1cbe39a13c5a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 896,
            "y": 672,
            "speed": 100
        },
        {
            "id": "9ae9b531-f1bd-400e-8879-50ff4a1e9f36",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 704,
            "speed": 100
        },
        {
            "id": "90915e32-587e-44e6-a653-575d8100bf01",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 704,
            "speed": 100
        },
        {
            "id": "b6cc33c5-29a3-42a9-b89f-075776c23430",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 544,
            "speed": 100
        },
        {
            "id": "4966827f-a3b0-4e63-8550-975138869cbb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 544,
            "speed": 100
        },
        {
            "id": "0a3b894b-03b2-4035-8118-94d47a0330f6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 576,
            "speed": 100
        },
        {
            "id": "bc2f1e4a-9958-4931-85ce-ca4025eb33a7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 640,
            "speed": 100
        },
        {
            "id": "58d40cb2-18e9-4fd6-95c3-fbc700ac76fc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 704,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}