{
    "id": "3e80e347-d332-4821-b5ef-d8d0bfea4ea0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_torch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 8,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "67904ee2-a324-4235-af51-183486e524c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e80e347-d332-4821-b5ef-d8d0bfea4ea0",
            "compositeImage": {
                "id": "fd9453b4-1cdd-45b7-9db4-452eca040573",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67904ee2-a324-4235-af51-183486e524c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d078b73-f2f2-493a-a05f-0e989d9729dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67904ee2-a324-4235-af51-183486e524c5",
                    "LayerId": "c81e3a4e-6233-4ae3-b709-dd27c1d72682"
                }
            ]
        },
        {
            "id": "2723e626-4bf1-4687-a3e3-9c416e12f70e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e80e347-d332-4821-b5ef-d8d0bfea4ea0",
            "compositeImage": {
                "id": "e0037780-950a-4908-ae56-63d856430c6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2723e626-4bf1-4687-a3e3-9c416e12f70e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d6b8772-02e0-45ea-93b5-15be0af7a02b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2723e626-4bf1-4687-a3e3-9c416e12f70e",
                    "LayerId": "c81e3a4e-6233-4ae3-b709-dd27c1d72682"
                }
            ]
        },
        {
            "id": "1f775d5a-3bd4-40b6-a607-e3446d80518c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e80e347-d332-4821-b5ef-d8d0bfea4ea0",
            "compositeImage": {
                "id": "13642459-596d-4667-b7f4-77b16fbe1441",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f775d5a-3bd4-40b6-a607-e3446d80518c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7d4f429-aab1-42d7-819f-b7f3bc5da74c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f775d5a-3bd4-40b6-a607-e3446d80518c",
                    "LayerId": "c81e3a4e-6233-4ae3-b709-dd27c1d72682"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c81e3a4e-6233-4ae3-b709-dd27c1d72682",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e80e347-d332-4821-b5ef-d8d0bfea4ea0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}