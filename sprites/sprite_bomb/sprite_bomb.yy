{
    "id": "21eb7f02-b739-469f-b331-9eeb7a7d0f41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_bomb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9cdcf949-8d23-4e06-b8a8-8d30412b9da1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21eb7f02-b739-469f-b331-9eeb7a7d0f41",
            "compositeImage": {
                "id": "25759019-ebf4-4736-9585-81476a925508",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cdcf949-8d23-4e06-b8a8-8d30412b9da1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "363948c1-5f37-476a-a2ed-433c61c93a85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cdcf949-8d23-4e06-b8a8-8d30412b9da1",
                    "LayerId": "1e4f0ad2-1cc7-4f54-ae21-88d792640a54"
                }
            ]
        },
        {
            "id": "c0312961-d92d-472c-83ab-2348feab54ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21eb7f02-b739-469f-b331-9eeb7a7d0f41",
            "compositeImage": {
                "id": "1341e0ee-f70d-4252-b46f-d565fbce3f91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0312961-d92d-472c-83ab-2348feab54ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2804721-4779-4a5e-ba7a-50e2574ba607",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0312961-d92d-472c-83ab-2348feab54ae",
                    "LayerId": "1e4f0ad2-1cc7-4f54-ae21-88d792640a54"
                }
            ]
        },
        {
            "id": "4f8bd98d-66fa-49bb-84f4-7de1fee66465",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21eb7f02-b739-469f-b331-9eeb7a7d0f41",
            "compositeImage": {
                "id": "8fddb468-4e28-4253-98fd-c0e0e55973da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f8bd98d-66fa-49bb-84f4-7de1fee66465",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fee39e85-de19-44b1-8469-de4867721298",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f8bd98d-66fa-49bb-84f4-7de1fee66465",
                    "LayerId": "1e4f0ad2-1cc7-4f54-ae21-88d792640a54"
                }
            ]
        },
        {
            "id": "f60a9922-fd80-47b4-a445-d8d83cbb9bc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21eb7f02-b739-469f-b331-9eeb7a7d0f41",
            "compositeImage": {
                "id": "37b4d6db-d077-42c1-9dde-85eb3ddef32a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f60a9922-fd80-47b4-a445-d8d83cbb9bc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71900902-9e8b-4c21-b3f8-794ad6636bd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f60a9922-fd80-47b4-a445-d8d83cbb9bc6",
                    "LayerId": "1e4f0ad2-1cc7-4f54-ae21-88d792640a54"
                }
            ]
        },
        {
            "id": "e1691a8a-a512-49b1-a96c-41dddb93781b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21eb7f02-b739-469f-b331-9eeb7a7d0f41",
            "compositeImage": {
                "id": "4cd576dd-caf7-41ed-8a2b-8eef0a11d1c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1691a8a-a512-49b1-a96c-41dddb93781b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3951967e-7416-45fc-9eca-d4322b73b15b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1691a8a-a512-49b1-a96c-41dddb93781b",
                    "LayerId": "1e4f0ad2-1cc7-4f54-ae21-88d792640a54"
                }
            ]
        },
        {
            "id": "ddfefe1c-d158-4211-bb7a-e8f842be5258",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21eb7f02-b739-469f-b331-9eeb7a7d0f41",
            "compositeImage": {
                "id": "50aa6d6d-c0d5-46b2-b2f1-62a4c76ae497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddfefe1c-d158-4211-bb7a-e8f842be5258",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "531ffd3d-d344-4861-a9ef-32596e51f6ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddfefe1c-d158-4211-bb7a-e8f842be5258",
                    "LayerId": "1e4f0ad2-1cc7-4f54-ae21-88d792640a54"
                }
            ]
        },
        {
            "id": "1400de0a-9e3a-4b56-af63-cf9306ff0cfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21eb7f02-b739-469f-b331-9eeb7a7d0f41",
            "compositeImage": {
                "id": "87614b43-ba28-4861-a020-d3b03f2c55d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1400de0a-9e3a-4b56-af63-cf9306ff0cfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2489bbbe-d581-4312-86c4-2e4c54c089bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1400de0a-9e3a-4b56-af63-cf9306ff0cfe",
                    "LayerId": "1e4f0ad2-1cc7-4f54-ae21-88d792640a54"
                }
            ]
        },
        {
            "id": "6eb2d02e-732e-436d-b292-b29742438a30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21eb7f02-b739-469f-b331-9eeb7a7d0f41",
            "compositeImage": {
                "id": "01cfc81b-bfaf-46cc-b852-daaff09b4a17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eb2d02e-732e-436d-b292-b29742438a30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ad0b8a8-dcaa-439e-80c5-c40c1e2a940f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eb2d02e-732e-436d-b292-b29742438a30",
                    "LayerId": "1e4f0ad2-1cc7-4f54-ae21-88d792640a54"
                }
            ]
        },
        {
            "id": "0991e760-758b-4ee1-a110-f2eb7ba3e24d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21eb7f02-b739-469f-b331-9eeb7a7d0f41",
            "compositeImage": {
                "id": "65d2d712-6246-4dc5-9f55-6d09dff608a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0991e760-758b-4ee1-a110-f2eb7ba3e24d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "960159eb-2851-43b9-bc2b-bf867f409288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0991e760-758b-4ee1-a110-f2eb7ba3e24d",
                    "LayerId": "1e4f0ad2-1cc7-4f54-ae21-88d792640a54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1e4f0ad2-1cc7-4f54-ae21-88d792640a54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21eb7f02-b739-469f-b331-9eeb7a7d0f41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}