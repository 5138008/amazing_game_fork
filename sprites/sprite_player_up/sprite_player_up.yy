{
    "id": "0b68c455-2fb5-4267-ba2b-de22ea97c63b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 2,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03c32504-e30f-4245-8702-8d7e70575535",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b68c455-2fb5-4267-ba2b-de22ea97c63b",
            "compositeImage": {
                "id": "3ebeefa4-91b0-4ad2-96b0-f248f7deb8bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03c32504-e30f-4245-8702-8d7e70575535",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50aeae9c-0f01-425f-8f62-28cc26740e30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03c32504-e30f-4245-8702-8d7e70575535",
                    "LayerId": "a1eaebce-0484-459b-9a4e-54cdd4f63e6f"
                }
            ]
        },
        {
            "id": "02b813c8-12df-4600-a370-d7dd4a198d20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b68c455-2fb5-4267-ba2b-de22ea97c63b",
            "compositeImage": {
                "id": "266107c0-36fd-40f5-b5df-95fe9567c2a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02b813c8-12df-4600-a370-d7dd4a198d20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39a4b00e-3aa4-4f23-a61f-1ef59b2e9494",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02b813c8-12df-4600-a370-d7dd4a198d20",
                    "LayerId": "a1eaebce-0484-459b-9a4e-54cdd4f63e6f"
                }
            ]
        },
        {
            "id": "2bd7a83f-6647-4775-8171-e06ee161b7d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b68c455-2fb5-4267-ba2b-de22ea97c63b",
            "compositeImage": {
                "id": "65a8b816-3bfe-4b0c-ba28-051bcbc2ed39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bd7a83f-6647-4775-8171-e06ee161b7d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0478c5d2-ab16-4790-875e-c5aa28fb4603",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bd7a83f-6647-4775-8171-e06ee161b7d3",
                    "LayerId": "a1eaebce-0484-459b-9a4e-54cdd4f63e6f"
                }
            ]
        },
        {
            "id": "2edf8360-8b35-41f6-8613-d4bda0e4ad22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b68c455-2fb5-4267-ba2b-de22ea97c63b",
            "compositeImage": {
                "id": "d22ef450-29e6-4710-bddd-2ac21844abad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2edf8360-8b35-41f6-8613-d4bda0e4ad22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83e1202a-4200-4188-be57-374ae8611167",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2edf8360-8b35-41f6-8613-d4bda0e4ad22",
                    "LayerId": "a1eaebce-0484-459b-9a4e-54cdd4f63e6f"
                }
            ]
        },
        {
            "id": "e6f0c67f-e878-4967-a209-3a93edff3ec6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b68c455-2fb5-4267-ba2b-de22ea97c63b",
            "compositeImage": {
                "id": "dd095dac-012e-4712-bdef-d0a22f4e4c4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6f0c67f-e878-4967-a209-3a93edff3ec6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a26b1ac-9092-44ce-9dd4-882d6117a3e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6f0c67f-e878-4967-a209-3a93edff3ec6",
                    "LayerId": "a1eaebce-0484-459b-9a4e-54cdd4f63e6f"
                }
            ]
        },
        {
            "id": "5723bf88-1370-4217-855c-c4b948deb253",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b68c455-2fb5-4267-ba2b-de22ea97c63b",
            "compositeImage": {
                "id": "c23e05bc-e921-4ecd-9f5d-febf7a99f727",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5723bf88-1370-4217-855c-c4b948deb253",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4922d1a-9bb3-4323-96b4-3dfd35f7e6e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5723bf88-1370-4217-855c-c4b948deb253",
                    "LayerId": "a1eaebce-0484-459b-9a4e-54cdd4f63e6f"
                }
            ]
        },
        {
            "id": "96edda55-97d7-41ce-8b87-87b856a8a6e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b68c455-2fb5-4267-ba2b-de22ea97c63b",
            "compositeImage": {
                "id": "18c0d45d-1b4c-4dd5-bec4-c19d081cdda9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96edda55-97d7-41ce-8b87-87b856a8a6e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "425c41f3-96f9-4c77-bee8-f9c94c396b45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96edda55-97d7-41ce-8b87-87b856a8a6e7",
                    "LayerId": "a1eaebce-0484-459b-9a4e-54cdd4f63e6f"
                }
            ]
        },
        {
            "id": "12c7d212-4e58-48b6-8d7c-63f4146fc496",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b68c455-2fb5-4267-ba2b-de22ea97c63b",
            "compositeImage": {
                "id": "f06f6175-7932-4498-839b-713c80f9f85a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12c7d212-4e58-48b6-8d7c-63f4146fc496",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a49679fe-13b3-48bf-b5a4-e212b31bf167",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12c7d212-4e58-48b6-8d7c-63f4146fc496",
                    "LayerId": "a1eaebce-0484-459b-9a4e-54cdd4f63e6f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "a1eaebce-0484-459b-9a4e-54cdd4f63e6f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b68c455-2fb5-4267-ba2b-de22ea97c63b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 0
}