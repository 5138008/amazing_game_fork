{
    "id": "754c1ee0-8517-4ec8-b61a-693bc9bf9416",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_wall_blocks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9613abc-f810-4556-99ca-75df07bfcb36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "754c1ee0-8517-4ec8-b61a-693bc9bf9416",
            "compositeImage": {
                "id": "e028bb4e-a794-4665-b499-07a4c7d7a64a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9613abc-f810-4556-99ca-75df07bfcb36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9661c148-2b73-4299-b11a-901f0bda0c89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9613abc-f810-4556-99ca-75df07bfcb36",
                    "LayerId": "b747783e-9ae4-4bdb-9ea7-cc4a22fa5e19"
                }
            ]
        },
        {
            "id": "e1127b08-3a7e-4f0e-870a-f755ad676d0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "754c1ee0-8517-4ec8-b61a-693bc9bf9416",
            "compositeImage": {
                "id": "e0c4fad1-33fe-44a4-934d-30a1777bd05a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1127b08-3a7e-4f0e-870a-f755ad676d0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "818d2e27-60a9-49ad-b2fe-4339a3122bba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1127b08-3a7e-4f0e-870a-f755ad676d0b",
                    "LayerId": "b747783e-9ae4-4bdb-9ea7-cc4a22fa5e19"
                }
            ]
        },
        {
            "id": "c08a5646-5262-408b-8916-46cf01e6e85b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "754c1ee0-8517-4ec8-b61a-693bc9bf9416",
            "compositeImage": {
                "id": "7cffa725-03d0-44a2-8d94-5fdf4f01ecbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c08a5646-5262-408b-8916-46cf01e6e85b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a47f596f-4723-497d-b22b-5cefbb6e1342",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c08a5646-5262-408b-8916-46cf01e6e85b",
                    "LayerId": "b747783e-9ae4-4bdb-9ea7-cc4a22fa5e19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b747783e-9ae4-4bdb-9ea7-cc4a22fa5e19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "754c1ee0-8517-4ec8-b61a-693bc9bf9416",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}