{
    "id": "1a2d1e0d-70ad-4f9c-89c9-491da146c8ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_red_switch",
    "For3D": true,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "305dc3f5-4ea4-4554-b0c5-8ec982e084be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a2d1e0d-70ad-4f9c-89c9-491da146c8ec",
            "compositeImage": {
                "id": "99ced8ff-9a8f-4464-9802-d9be572247d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "305dc3f5-4ea4-4554-b0c5-8ec982e084be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b87bf5ea-cbe6-487d-81ce-f283162723d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "305dc3f5-4ea4-4554-b0c5-8ec982e084be",
                    "LayerId": "d9f884cd-b80e-4efb-948c-d799917f4ea5"
                }
            ]
        },
        {
            "id": "defecced-2504-4ba6-9ea4-aa93233861f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a2d1e0d-70ad-4f9c-89c9-491da146c8ec",
            "compositeImage": {
                "id": "d1760f8a-2dcf-4244-83dc-c700151c4f2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "defecced-2504-4ba6-9ea4-aa93233861f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c891d55-5d15-44c1-b6fc-3d50ae8db0d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "defecced-2504-4ba6-9ea4-aa93233861f2",
                    "LayerId": "d9f884cd-b80e-4efb-948c-d799917f4ea5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d9f884cd-b80e-4efb-948c-d799917f4ea5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a2d1e0d-70ad-4f9c-89c9-491da146c8ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}