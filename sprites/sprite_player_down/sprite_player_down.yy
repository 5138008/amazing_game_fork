{
    "id": "c7a4fb25-0a48-442e-9695-379f30c412e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 2,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df3316b2-49df-45c5-a843-a5a55f96d2ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7a4fb25-0a48-442e-9695-379f30c412e8",
            "compositeImage": {
                "id": "8ff33094-a721-4c94-9fe6-cfd8fe448a94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df3316b2-49df-45c5-a843-a5a55f96d2ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88882fa6-268f-4e10-ad4c-283ebd95f49e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df3316b2-49df-45c5-a843-a5a55f96d2ee",
                    "LayerId": "aca8eef1-d479-4d7c-8c7b-3f91fa71be1b"
                }
            ]
        },
        {
            "id": "03c5a4c1-2e2a-4c9c-b694-f12d8ccf731e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7a4fb25-0a48-442e-9695-379f30c412e8",
            "compositeImage": {
                "id": "2857801a-84d2-4fd4-9ca0-fc65eb3731bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03c5a4c1-2e2a-4c9c-b694-f12d8ccf731e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59f81c3a-9f3e-4088-969c-c813ff16432e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03c5a4c1-2e2a-4c9c-b694-f12d8ccf731e",
                    "LayerId": "aca8eef1-d479-4d7c-8c7b-3f91fa71be1b"
                }
            ]
        },
        {
            "id": "52e0a52c-79d9-411c-8254-74d43c45aad8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7a4fb25-0a48-442e-9695-379f30c412e8",
            "compositeImage": {
                "id": "54b6b9e8-8bac-44c0-9e85-b0a548fc4f1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52e0a52c-79d9-411c-8254-74d43c45aad8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7931db0-ff87-436e-85cb-65e7c14224cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e0a52c-79d9-411c-8254-74d43c45aad8",
                    "LayerId": "aca8eef1-d479-4d7c-8c7b-3f91fa71be1b"
                }
            ]
        },
        {
            "id": "e314ab80-bd91-46d7-a8a6-9d51814802b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7a4fb25-0a48-442e-9695-379f30c412e8",
            "compositeImage": {
                "id": "b4df957e-573b-4543-9a8c-0faba46f1ed6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e314ab80-bd91-46d7-a8a6-9d51814802b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c406b32c-35d2-4c6a-9158-e152042672fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e314ab80-bd91-46d7-a8a6-9d51814802b3",
                    "LayerId": "aca8eef1-d479-4d7c-8c7b-3f91fa71be1b"
                }
            ]
        },
        {
            "id": "eb510d50-7f5b-4346-b24c-cae0801b8d31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7a4fb25-0a48-442e-9695-379f30c412e8",
            "compositeImage": {
                "id": "532f6374-b979-464b-8f31-00d6cbe0129b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb510d50-7f5b-4346-b24c-cae0801b8d31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f14ea4e-c104-4e9e-a5f2-d50dc48e18fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb510d50-7f5b-4346-b24c-cae0801b8d31",
                    "LayerId": "aca8eef1-d479-4d7c-8c7b-3f91fa71be1b"
                }
            ]
        },
        {
            "id": "c5ff43f7-a23b-4ec1-8fa2-a74585ca2080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7a4fb25-0a48-442e-9695-379f30c412e8",
            "compositeImage": {
                "id": "1e7b0e0a-83d1-41ab-9466-4584edbfe6ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5ff43f7-a23b-4ec1-8fa2-a74585ca2080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2892a654-4965-488a-861a-43ac404202d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5ff43f7-a23b-4ec1-8fa2-a74585ca2080",
                    "LayerId": "aca8eef1-d479-4d7c-8c7b-3f91fa71be1b"
                }
            ]
        },
        {
            "id": "5ce3a40c-2cab-4f90-929a-e6673d509f0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7a4fb25-0a48-442e-9695-379f30c412e8",
            "compositeImage": {
                "id": "46412615-61e4-4d9d-855e-a6b6dfdac644",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ce3a40c-2cab-4f90-929a-e6673d509f0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a98d40f3-784c-4f84-a82f-d31de1061b51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ce3a40c-2cab-4f90-929a-e6673d509f0a",
                    "LayerId": "aca8eef1-d479-4d7c-8c7b-3f91fa71be1b"
                }
            ]
        },
        {
            "id": "3bcf539d-f566-4d94-ad0e-573d22832e54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7a4fb25-0a48-442e-9695-379f30c412e8",
            "compositeImage": {
                "id": "1171b8fc-2b6a-4de5-a1e5-57e6cd381a88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bcf539d-f566-4d94-ad0e-573d22832e54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a56ec177-d02f-448c-820d-ae66acf74a0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bcf539d-f566-4d94-ad0e-573d22832e54",
                    "LayerId": "aca8eef1-d479-4d7c-8c7b-3f91fa71be1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "aca8eef1-d479-4d7c-8c7b-3f91fa71be1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7a4fb25-0a48-442e-9695-379f30c412e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 0
}