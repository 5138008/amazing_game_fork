{
    "id": "3353e299-1d8d-45c8-82e0-d61eb457b892",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 2,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34cefddf-6415-42d5-806a-4171528e5311",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3353e299-1d8d-45c8-82e0-d61eb457b892",
            "compositeImage": {
                "id": "c24e5797-3537-4d3e-b1b7-11b03f6fb8d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34cefddf-6415-42d5-806a-4171528e5311",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66152563-1fa1-4247-8902-dc91ffe25c06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34cefddf-6415-42d5-806a-4171528e5311",
                    "LayerId": "cd3ff781-bc80-454f-8e4b-02cea20989b2"
                }
            ]
        },
        {
            "id": "0e3efceb-f976-4a82-9cc8-9a80be30b76a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3353e299-1d8d-45c8-82e0-d61eb457b892",
            "compositeImage": {
                "id": "57553df4-4965-4223-a8af-06599435b6e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e3efceb-f976-4a82-9cc8-9a80be30b76a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cdd6dfb-6ca6-4b96-b160-7c22dc6de885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e3efceb-f976-4a82-9cc8-9a80be30b76a",
                    "LayerId": "cd3ff781-bc80-454f-8e4b-02cea20989b2"
                }
            ]
        },
        {
            "id": "f889210a-997e-44bf-a920-c53485f90d65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3353e299-1d8d-45c8-82e0-d61eb457b892",
            "compositeImage": {
                "id": "1f139519-4fb0-43a1-afd1-5b1fd72b279a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f889210a-997e-44bf-a920-c53485f90d65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdb7ff0c-3fba-4393-a5ee-ec5cf493fa23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f889210a-997e-44bf-a920-c53485f90d65",
                    "LayerId": "cd3ff781-bc80-454f-8e4b-02cea20989b2"
                }
            ]
        },
        {
            "id": "ce58f4b9-04dd-4466-8bdb-4197377c1a1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3353e299-1d8d-45c8-82e0-d61eb457b892",
            "compositeImage": {
                "id": "8370aab3-d535-4a36-94a5-b1a9b15ae68f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce58f4b9-04dd-4466-8bdb-4197377c1a1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dfd4129-e2fb-4d05-93f8-1ef4974499fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce58f4b9-04dd-4466-8bdb-4197377c1a1a",
                    "LayerId": "cd3ff781-bc80-454f-8e4b-02cea20989b2"
                }
            ]
        },
        {
            "id": "a565fd17-9eac-4ce6-a413-d7aae68fdcc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3353e299-1d8d-45c8-82e0-d61eb457b892",
            "compositeImage": {
                "id": "167acefc-fa88-4989-ba73-3c26573a4947",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a565fd17-9eac-4ce6-a413-d7aae68fdcc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32d3734f-34fe-4262-84b1-8bd9c23a6ab0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a565fd17-9eac-4ce6-a413-d7aae68fdcc0",
                    "LayerId": "cd3ff781-bc80-454f-8e4b-02cea20989b2"
                }
            ]
        },
        {
            "id": "0a7cb3fc-8dcb-49ce-b9a7-a1cfa48c9f34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3353e299-1d8d-45c8-82e0-d61eb457b892",
            "compositeImage": {
                "id": "9d0b55f5-5a50-4211-a9ec-9c85f8d85c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a7cb3fc-8dcb-49ce-b9a7-a1cfa48c9f34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c64115f6-b3a4-438e-98f5-a9ee4812fb6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a7cb3fc-8dcb-49ce-b9a7-a1cfa48c9f34",
                    "LayerId": "cd3ff781-bc80-454f-8e4b-02cea20989b2"
                }
            ]
        },
        {
            "id": "2753ac16-c1ce-48e7-afe7-0fffe7eae8ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3353e299-1d8d-45c8-82e0-d61eb457b892",
            "compositeImage": {
                "id": "257be1da-2049-4e25-a5eb-de5b2ab1d70f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2753ac16-c1ce-48e7-afe7-0fffe7eae8ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2dd6f92-ce88-4537-ba96-6c2ecffeea03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2753ac16-c1ce-48e7-afe7-0fffe7eae8ec",
                    "LayerId": "cd3ff781-bc80-454f-8e4b-02cea20989b2"
                }
            ]
        },
        {
            "id": "3d8bee49-4b9c-45af-919e-5b1dd8725bfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3353e299-1d8d-45c8-82e0-d61eb457b892",
            "compositeImage": {
                "id": "7c6e96fa-b452-469c-96e5-0c86a2d52ec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d8bee49-4b9c-45af-919e-5b1dd8725bfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18fc8208-bba6-4117-a0d6-443b9ab49dd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d8bee49-4b9c-45af-919e-5b1dd8725bfa",
                    "LayerId": "cd3ff781-bc80-454f-8e4b-02cea20989b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "cd3ff781-bc80-454f-8e4b-02cea20989b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3353e299-1d8d-45c8-82e0-d61eb457b892",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 0
}