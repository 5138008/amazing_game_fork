{
    "id": "0a98feab-c91a-40cb-9ecd-72b89e9d2589",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_statue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5975285-045f-45fc-b631-e65580fe4e86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a98feab-c91a-40cb-9ecd-72b89e9d2589",
            "compositeImage": {
                "id": "be99ccb5-d8c9-477c-85eb-4fb213bb453a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5975285-045f-45fc-b631-e65580fe4e86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e16ebf4-33ab-43f7-8b26-60f350d544bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5975285-045f-45fc-b631-e65580fe4e86",
                    "LayerId": "2243fa24-eef1-4769-a8e2-e858dd97597d"
                }
            ]
        },
        {
            "id": "fb0e1240-45df-45bc-8dd3-226ce049bde7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a98feab-c91a-40cb-9ecd-72b89e9d2589",
            "compositeImage": {
                "id": "7e0d3016-d5ae-42b5-84c5-224b3ac4f0d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb0e1240-45df-45bc-8dd3-226ce049bde7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "781b693c-8a1b-4cb0-8e6a-66437ece4e92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb0e1240-45df-45bc-8dd3-226ce049bde7",
                    "LayerId": "2243fa24-eef1-4769-a8e2-e858dd97597d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2243fa24-eef1-4769-a8e2-e858dd97597d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a98feab-c91a-40cb-9ecd-72b89e9d2589",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}