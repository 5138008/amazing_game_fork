{
    "id": "06040f06-d926-4e9c-9172-568db4252b60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_skeleton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b835b97d-7394-4f6b-98a7-1220d595720b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06040f06-d926-4e9c-9172-568db4252b60",
            "compositeImage": {
                "id": "8131ac10-651f-473b-9438-4f157074e0dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b835b97d-7394-4f6b-98a7-1220d595720b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50998d82-7113-4998-8ed1-326433911533",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b835b97d-7394-4f6b-98a7-1220d595720b",
                    "LayerId": "43435b8e-eabf-4584-ac1d-73604a93835e"
                }
            ]
        },
        {
            "id": "8d173086-e70b-434e-ad13-83f98a1e63d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06040f06-d926-4e9c-9172-568db4252b60",
            "compositeImage": {
                "id": "4576e669-3eab-4cfc-9647-9724183cacb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d173086-e70b-434e-ad13-83f98a1e63d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "383557ed-59cb-496f-913e-770ffd949af0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d173086-e70b-434e-ad13-83f98a1e63d1",
                    "LayerId": "43435b8e-eabf-4584-ac1d-73604a93835e"
                }
            ]
        },
        {
            "id": "454b4349-c471-49cf-a6e6-b2d389666295",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06040f06-d926-4e9c-9172-568db4252b60",
            "compositeImage": {
                "id": "115459c9-043a-433d-81da-f95f3a0a9ea3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "454b4349-c471-49cf-a6e6-b2d389666295",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "795383e9-d7f5-402d-a1ce-386f7af528aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "454b4349-c471-49cf-a6e6-b2d389666295",
                    "LayerId": "43435b8e-eabf-4584-ac1d-73604a93835e"
                }
            ]
        },
        {
            "id": "67aa8c5d-e5ad-48e1-9e55-fd74de0795df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06040f06-d926-4e9c-9172-568db4252b60",
            "compositeImage": {
                "id": "72438d6b-e300-4421-9fbe-7f07444da7d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67aa8c5d-e5ad-48e1-9e55-fd74de0795df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "871ee322-15ea-441f-8d18-7d90c2da220b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67aa8c5d-e5ad-48e1-9e55-fd74de0795df",
                    "LayerId": "43435b8e-eabf-4584-ac1d-73604a93835e"
                }
            ]
        },
        {
            "id": "3f25392a-8b11-4c1d-95fa-f24a5ad210e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06040f06-d926-4e9c-9172-568db4252b60",
            "compositeImage": {
                "id": "e1fe0416-d6de-4aeb-9b62-e488d3199988",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f25392a-8b11-4c1d-95fa-f24a5ad210e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b10b1fb5-123d-444e-a699-eed219f97d03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f25392a-8b11-4c1d-95fa-f24a5ad210e9",
                    "LayerId": "43435b8e-eabf-4584-ac1d-73604a93835e"
                }
            ]
        },
        {
            "id": "639a9ffd-6755-4929-bd12-74588f74683a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06040f06-d926-4e9c-9172-568db4252b60",
            "compositeImage": {
                "id": "609388c0-5fea-44ea-99ed-fd31a7abc2b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "639a9ffd-6755-4929-bd12-74588f74683a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81140a86-a02f-4ef2-bfb6-61f5a7463a35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "639a9ffd-6755-4929-bd12-74588f74683a",
                    "LayerId": "43435b8e-eabf-4584-ac1d-73604a93835e"
                }
            ]
        },
        {
            "id": "aee6bf1c-a547-4be2-94a9-6b7d46957099",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06040f06-d926-4e9c-9172-568db4252b60",
            "compositeImage": {
                "id": "fad1c38d-0019-48eb-9e1f-fba533472183",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aee6bf1c-a547-4be2-94a9-6b7d46957099",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b504f18-4757-48c1-87b3-16873a587685",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aee6bf1c-a547-4be2-94a9-6b7d46957099",
                    "LayerId": "43435b8e-eabf-4584-ac1d-73604a93835e"
                }
            ]
        },
        {
            "id": "1932bdb0-c313-473a-9cab-9872831df68a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06040f06-d926-4e9c-9172-568db4252b60",
            "compositeImage": {
                "id": "d0309d83-ffbe-4748-8887-0ce8a82069be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1932bdb0-c313-473a-9cab-9872831df68a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3902ded-944f-474e-b219-f7bf3eae4e2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1932bdb0-c313-473a-9cab-9872831df68a",
                    "LayerId": "43435b8e-eabf-4584-ac1d-73604a93835e"
                }
            ]
        },
        {
            "id": "3b4978dd-9cbd-4e6a-a040-bcb153ecbad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06040f06-d926-4e9c-9172-568db4252b60",
            "compositeImage": {
                "id": "7964eaab-6c71-45dd-9de5-35c64d2ab8ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b4978dd-9cbd-4e6a-a040-bcb153ecbad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41778487-8b50-43d6-ba04-758687262f73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b4978dd-9cbd-4e6a-a040-bcb153ecbad6",
                    "LayerId": "43435b8e-eabf-4584-ac1d-73604a93835e"
                }
            ]
        },
        {
            "id": "1582c620-896f-454e-b5f0-0168cebb5073",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06040f06-d926-4e9c-9172-568db4252b60",
            "compositeImage": {
                "id": "08da7cb4-ebf2-4db3-b926-cef372fa9053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1582c620-896f-454e-b5f0-0168cebb5073",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09f511fd-bcf0-493f-9f90-727618a0abdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1582c620-896f-454e-b5f0-0168cebb5073",
                    "LayerId": "43435b8e-eabf-4584-ac1d-73604a93835e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "43435b8e-eabf-4584-ac1d-73604a93835e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06040f06-d926-4e9c-9172-568db4252b60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}