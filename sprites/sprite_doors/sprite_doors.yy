{
    "id": "f4c03e0d-4b2a-4733-8d15-d148e01f7480",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_doors",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8545322c-a585-4fe6-b3ff-160cedbc5966",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4c03e0d-4b2a-4733-8d15-d148e01f7480",
            "compositeImage": {
                "id": "a0a6c0cb-850b-40ab-a477-59c046b58517",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8545322c-a585-4fe6-b3ff-160cedbc5966",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa18c019-e9e8-4eee-82f4-f2cfc1e1180e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8545322c-a585-4fe6-b3ff-160cedbc5966",
                    "LayerId": "fe222451-a886-44ef-b001-4fda7a2d9190"
                }
            ]
        },
        {
            "id": "cbf61f43-dccc-4bd2-a9a7-8b0c50b55761",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4c03e0d-4b2a-4733-8d15-d148e01f7480",
            "compositeImage": {
                "id": "e96f6866-beee-4c48-b7e8-8ed5f2db60ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbf61f43-dccc-4bd2-a9a7-8b0c50b55761",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f0a3af2-5867-4411-97c1-ff3ba664c5fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbf61f43-dccc-4bd2-a9a7-8b0c50b55761",
                    "LayerId": "fe222451-a886-44ef-b001-4fda7a2d9190"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fe222451-a886-44ef-b001-4fda7a2d9190",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4c03e0d-4b2a-4733-8d15-d148e01f7480",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}