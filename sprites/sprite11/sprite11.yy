{
    "id": "c0733b4f-c0c2-4cfd-9021-1babdbe621ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c53476a0-57df-43af-a5ee-4670d31d50cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0733b4f-c0c2-4cfd-9021-1babdbe621ad",
            "compositeImage": {
                "id": "52ded4c3-156a-4c4b-8566-2f40ffbe484c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c53476a0-57df-43af-a5ee-4670d31d50cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68fd4069-7e60-4368-8883-c9f95108fde9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c53476a0-57df-43af-a5ee-4670d31d50cf",
                    "LayerId": "0b805104-7fa9-4c47-9a99-233f040ee702"
                }
            ]
        },
        {
            "id": "8e6564ad-6ede-466e-9f2b-b89561ff9e95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0733b4f-c0c2-4cfd-9021-1babdbe621ad",
            "compositeImage": {
                "id": "2a17afd5-f913-4e3e-9856-e45b53f4c1da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e6564ad-6ede-466e-9f2b-b89561ff9e95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c70f403-1262-43e4-88ff-5fd437b2aaff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e6564ad-6ede-466e-9f2b-b89561ff9e95",
                    "LayerId": "0b805104-7fa9-4c47-9a99-233f040ee702"
                }
            ]
        },
        {
            "id": "20e1734f-cff6-44fa-9084-73108335c001",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0733b4f-c0c2-4cfd-9021-1babdbe621ad",
            "compositeImage": {
                "id": "ed4360bf-0fb8-4f7a-b864-0bdb69739507",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20e1734f-cff6-44fa-9084-73108335c001",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6653f5df-3915-4a82-abe0-a4b7dcefe14b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20e1734f-cff6-44fa-9084-73108335c001",
                    "LayerId": "0b805104-7fa9-4c47-9a99-233f040ee702"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0b805104-7fa9-4c47-9a99-233f040ee702",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0733b4f-c0c2-4cfd-9021-1babdbe621ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}