{
    "id": "437051a4-7aca-44d1-b4bd-e2b284a33abf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_waterfall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bde35b38-402b-45ba-b03c-8d4601f0fb54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "437051a4-7aca-44d1-b4bd-e2b284a33abf",
            "compositeImage": {
                "id": "08cd7b8f-11f1-4202-96e1-c3c68d1a16ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bde35b38-402b-45ba-b03c-8d4601f0fb54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ef5058a-b51f-42b7-8d4e-c4bdf876cbf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bde35b38-402b-45ba-b03c-8d4601f0fb54",
                    "LayerId": "5d928fd9-0f1a-488c-a61b-571774ade5fb"
                }
            ]
        },
        {
            "id": "4b3c08be-1c43-420b-8040-4059329d3831",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "437051a4-7aca-44d1-b4bd-e2b284a33abf",
            "compositeImage": {
                "id": "c528bed7-d534-4f33-9dd5-3b6a6b5d854b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b3c08be-1c43-420b-8040-4059329d3831",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6743c48-b0ac-4ea3-9905-ba3e4cda1620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b3c08be-1c43-420b-8040-4059329d3831",
                    "LayerId": "5d928fd9-0f1a-488c-a61b-571774ade5fb"
                }
            ]
        },
        {
            "id": "0b0b0231-a9c4-45ad-9003-38731d0c8e2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "437051a4-7aca-44d1-b4bd-e2b284a33abf",
            "compositeImage": {
                "id": "f867ed63-026c-4580-87c7-9fade71ba295",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b0b0231-a9c4-45ad-9003-38731d0c8e2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dcce014-4c63-4090-9fd5-1bae97d1eafc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b0b0231-a9c4-45ad-9003-38731d0c8e2d",
                    "LayerId": "5d928fd9-0f1a-488c-a61b-571774ade5fb"
                }
            ]
        },
        {
            "id": "534bf99f-0b75-44e9-8536-d9542f29c518",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "437051a4-7aca-44d1-b4bd-e2b284a33abf",
            "compositeImage": {
                "id": "602083ef-60f7-47c8-8035-68057546d286",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "534bf99f-0b75-44e9-8536-d9542f29c518",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9029eae-b9d9-442b-8b6a-31589d8ba317",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "534bf99f-0b75-44e9-8536-d9542f29c518",
                    "LayerId": "5d928fd9-0f1a-488c-a61b-571774ade5fb"
                }
            ]
        },
        {
            "id": "261c4d83-adc9-4dd9-806c-743046ae5061",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "437051a4-7aca-44d1-b4bd-e2b284a33abf",
            "compositeImage": {
                "id": "017561e0-b6f5-48a1-b2e7-8b96d36408fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "261c4d83-adc9-4dd9-806c-743046ae5061",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f4aec1f-1fcb-466c-a757-672a2175cf2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "261c4d83-adc9-4dd9-806c-743046ae5061",
                    "LayerId": "5d928fd9-0f1a-488c-a61b-571774ade5fb"
                }
            ]
        },
        {
            "id": "84dde870-33a0-49af-bf02-d7e3d5dabdb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "437051a4-7aca-44d1-b4bd-e2b284a33abf",
            "compositeImage": {
                "id": "7263e743-ba6d-4cff-a973-67bb7665315a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84dde870-33a0-49af-bf02-d7e3d5dabdb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf6c33c9-f283-458e-8bba-58a47c3d239b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84dde870-33a0-49af-bf02-d7e3d5dabdb0",
                    "LayerId": "5d928fd9-0f1a-488c-a61b-571774ade5fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "5d928fd9-0f1a-488c-a61b-571774ade5fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "437051a4-7aca-44d1-b4bd-e2b284a33abf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 0,
    "yorig": 0
}