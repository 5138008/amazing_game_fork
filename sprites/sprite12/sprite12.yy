{
    "id": "15184ab4-c493-4bd6-ad78-51db87f112c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite12",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 10,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc74d23e-b07c-410f-ac60-01ce9d2bce91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15184ab4-c493-4bd6-ad78-51db87f112c8",
            "compositeImage": {
                "id": "8347b79b-6253-472e-823b-c215899dac38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc74d23e-b07c-410f-ac60-01ce9d2bce91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a074856f-5c05-4779-8a05-96ac2105800d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc74d23e-b07c-410f-ac60-01ce9d2bce91",
                    "LayerId": "71bc2525-f4ea-4bc6-accb-30f428e04626"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "71bc2525-f4ea-4bc6-accb-30f428e04626",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15184ab4-c493-4bd6-ad78-51db87f112c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}